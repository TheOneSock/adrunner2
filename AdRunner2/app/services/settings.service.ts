import * as fs from 'fs';
import * as path from 'path';
import { Folder } from '../dtos/folder';
import { Injectable } from '@angular/core';
import { remote } from 'electron';
import { UserSettings } from '../dtos/user-setting';
import * as child from 'child_process';
import { PowershellCommandRunner } from "./powershell-comand-runner";

@Injectable()
export class SettingsService {

    private _userSettings: UserSettings;
    private _userSettingsPath: string = path.join(remote.app.getPath('userData'), 'userData.json');
    private _implementationsConfigPath: string = path.join(remote.app.getPath('userData'), 'ImplementationConfigs');
    private _implementationsConfigFiles: Folder[];
    public scriptPath: string = path.join(remote.app.getAppPath().replace('\app.asar', ''), "PowershellScripts");

    private _defaultUserSettings: UserSettings =
    {
        "applicationVersion": remote.app.getVersion(),
        "folders": []
    } as UserSettings;


    public getUserSettings(): UserSettings {

        if (!this._userSettings) {
            this._userSettings = this.loadUserSettings();
            if (!this._userSettings) {
                this._userSettings = this._defaultUserSettings;
                this.saveUserSettings(this._userSettings);
            }
        }
        //console.log(JSON.stringify(this._userSettings))
        return this._userSettings;
    }

    public addFolder(folder: Folder): void {
        console.log(JSON.stringify(folder))

        let index = this._userSettings.folders.find(function (value, index): boolean {
            return folder.Path === value.Path
        });
        if (index != undefined)
            return;

        this._userSettings.folders.push(folder);
        this.saveUserSettings(this._userSettings);
    }

    public getImplementationConfigFiles(): Folder[] {
        if (!this._implementationsConfigFiles) {

            this.createAndFillImplementationConfigsIfNotExists();

            let list: Folder[] = [];

            fs.readdirSync(this._implementationsConfigPath).forEach(fileName => {
                if (!fileName.endsWith(".xml"))
                    return;
                let file = path.resolve(this._implementationsConfigPath, fileName);
                let stat = fs.statSync(file);
                if (stat && stat.isFile()) {
                    let f = new Folder();
                    f.Name = fileName;
                    f.Path = file;
                    list.push(f)
                }
            });
            this._implementationsConfigFiles = list;
        }
        return this._implementationsConfigFiles;
    }

    private createAndFillImplementationConfigsIfNotExists(): void {
        if (fs.existsSync(this._implementationsConfigPath))
            return;
        let defaultConfigsPath = path.join(remote.app.getAppPath().replace('\app.asar',''), "ImplementationConfigs");
        child.execSync(PowershellCommandRunner.powershellCommandPrefix +'"cd \'' + this.scriptPath + '\';./copyDefaultImplementationConfigurations.ps1 \'' + defaultConfigsPath + '\' \'' + remote.app.getPath('userData') + '\'', );
    }

    public removeFolder(folder: Folder): void {
        console.log(JSON.stringify(folder))
        let index = this._userSettings.folders.indexOf(folder);
        this._userSettings.folders.splice(index, 1);
        this.saveUserSettings(this._userSettings);
    }

    private saveUserSettings(userSettings: UserSettings): void {
        fs.writeFileSync(this._userSettingsPath, JSON.stringify(userSettings));
    }

    private loadUserSettings(): UserSettings {
        if (!fs.existsSync(this._userSettingsPath)) {
            return null;
        }
        let userSettings = JSON.parse(fs.readFileSync(this._userSettingsPath, 'utf8')) as UserSettings;

        userSettings.folders = userSettings.folders.filter(function (folder) {
            return fs.existsSync(folder.Path);
        });
        userSettings.applicationVersion = remote.app.getVersion();

        this.saveUserSettings(userSettings);
        return userSettings;
    }
}
