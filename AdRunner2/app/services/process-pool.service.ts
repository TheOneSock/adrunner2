import { RunningProcess, ProcessArgs } from '../dtos/running-process';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Injectable, } from '@angular/core';

@Injectable()
export class ProcessPoolService {
    private processes: RunningProcess[] = [];
    private processesSubDict: { [id: number]: Subscription[]; } = {};

    private processDoneSource = new Subject<ProcessArgs>();
    public processDone$ = this.processDoneSource.asObservable();
    private processAddedSource = new Subject<number>();
    public processAdded$ = this.processAddedSource.asObservable();
    private processRemovedSource = new Subject<ProcessArgs>();
    public processRemoved$ = this.processRemovedSource.asObservable();
    private processStdoutSource = new Subject<number>();
    public processStdout$ = this.processStdoutSource.asObservable();
    private processPercentDoneSource = new Subject<number>();
    public processPercentDone$ = this.processPercentDoneSource.asObservable();
    private processStartedSource = new Subject<ProcessArgs>();
    public processStarted$ = this.processStartedSource.asObservable();
    private watchProcessSource = new Subject<number>();
    public watchProcess$ = this.watchProcessSource.asObservable();

    public AddProcess(process: RunningProcess) {
        this.processes.push(process);
        let isActiveChangedSubscription = process.isActiveChanged$.subscribe(pid => {
             this.processDoneSource.next(pid) 
             let tmp = this.GetProcess(pid.processId)
             if( tmp.isSuccessful && tmp.deleteOnSuccessful){
                 this.RemoveProcess(pid.processId);
             }
            })
        let stdoutSuscription = process.stdoutChanged$.subscribe(pid => { this.processStdoutSource.next(pid) })
        let percentDoneSuscription = process.percentDoneChanged$.subscribe(pid => { this.processPercentDoneSource.next(pid) })
        let isStartedSuscription = process.isStartedChanged$.subscribe(pid => { this.processStartedSource.next(pid) })

        this.processesSubDict[process.id] = [isActiveChangedSubscription, stdoutSuscription, percentDoneSuscription, isStartedSuscription];

        this.processAddedSource.next(process.id);
    }

    public RemoveProcess(id: number) {
        let subscriptions = this.processesSubDict[id];
        subscriptions.forEach(subscription => {
            subscription.unsubscribe();
        });

        this.processesSubDict[id] = null;

        let index = this.processes.findIndex(p => p.id == id);

        if (index > -1) {
            let pid = new ProcessArgs(this.processes[index].id, this.processes[index].checkoutId);
            this.processes.splice(index, 1);
            this.processRemovedSource.next(pid);
        }
    }

    public removeFinishedProcesses() {
        let runningProcesses = this.processes.map((p, i) => { let x: [RunningProcess, number] = [p, i]; return x; }).filter(kp => !kp[0].isActive);
        this.processes = this.processes.filter(p => p.isActive);
        runningProcesses.forEach((kp) => {

            let subscriptions = this.processesSubDict[kp[0].id];
            subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
            this.processesSubDict[kp[0].id] = null;

            this.processRemovedSource.next(new ProcessArgs(kp[0].id, kp[0].checkoutId));
        })
    }

    public WatchProcess(id: number) {
        this.watchProcessSource.next(id);
    }


    public GetProcess(id: number): RunningProcess {
        return this.processes.find(p => p.id == id);
    }



}