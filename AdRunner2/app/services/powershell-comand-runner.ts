import { PowershellTask } from "../dtos/psake-task";
import { RunningProcess } from "../dtos/running-process";
import { ProcessPoolService } from "./process-pool.service";
import * as child from 'child_process';
import * as path from 'path';
import { Subscription } from "rxjs/Subscription";
const { remote, nativeImage } = require('electron');



export class PowershellCommandRunner {
  public static powershellCommandPrefix: string = "powershell -NoProfile -ExecutionPolicy Bypass -Command ";
  private static _instance: PowershellCommandRunner = null;

  public static getInstance(processPoolService: ProcessPoolService) {
    if (!PowershellCommandRunner._instance)
      PowershellCommandRunner._instance = new PowershellCommandRunner(processPoolService);
    return PowershellCommandRunner._instance;
  }

  private constructor(private processPoolService: ProcessPoolService) {
    
    remote.app.on('browser-window-focus',()=>{
      var win = remote.getCurrentWindow();
      win.setProgressBar(-1);
    })
    this.processDoneSubscription = processPoolService.processDone$.subscribe((pid) => {
      this._runningTasksNum--;
      this._runningTasks[pid.checkoutId] = this._runningTasks[pid.checkoutId] - 1;

      if (this._taskBuffer[pid.checkoutId].any()) {
        let taskData = this._taskBuffer[pid.checkoutId].pop();
        this.runTaskData(taskData);
      } else {
        for (var key in this._taskBuffer) {
          var value = this._taskBuffer[key];
          if (value.any()) {
            this.runTaskData(value.pop());
            break;
          }
        }
      }

      this.setIconProgressBar();
    }
    );

    this.processRemovedSubscription = processPoolService.processRemoved$.subscribe((pid) => {
      if (this._taskBuffer[pid.checkoutId].any()) {
        let index = this._taskBuffer[pid.checkoutId].store.findIndex(p => p.process.id == pid.processId);

        if (index > -1) {
          this._taskBuffer[pid.checkoutId].store.splice(index, 1);
        }
      }
    });
  }

  private processDoneSubscription: Subscription;
  private processRemovedSubscription: Subscription;
  private maxPerCheckout: number = 1;
  private maxTotal: number = 6;


  private _taskBuffer: { [id: number]: Queue<TaskData> } = [];
  private _runningTasks: { [id: number]: number } = [];
  private _runningTasksNum: number = 0;


  public startPowershellTask(task: PowershellTask): RunningProcess {
    let process = new RunningProcess(task.name, task.originCheckout);
    this.processPoolService.AddProcess(process);
    process.deleteOnSuccessful = task.deleteProcessOnSuccessful;
    process.stdOut = "";
    process.percentDone = 0;

    let taskData: TaskData = new TaskData(task, process);

    if (!this._runningTasks[task.originCheckout])
      this._runningTasks[task.originCheckout] = 0;
    if (!this._runningTasks[task.originCheckout])
      this._taskBuffer[task.originCheckout] = new Queue<TaskData>();

    if (this._runningTasksNum >= this.maxTotal || this._runningTasks[task.originCheckout] >= this.maxPerCheckout) {
      //queue
      this._taskBuffer[task.originCheckout].push(taskData);
    } else {
      //run
      this.runTaskData(taskData);
    }

    return process;
  }

  private runTaskData(taskData: TaskData) {
    this._runningTasksNum++;
    this._runningTasks[taskData.task.originCheckout] = this._runningTasks[taskData.task.originCheckout] + 1;

    taskData.process.percentDone = 100;
    taskData.process.startTime = new Date();
    taskData.process.isStarted = true;


    console.log("running command (" + taskData.process.id + "): " + taskData.task.command);
    let powershellRequest = child.exec(taskData.task.command);
    taskData.process.childProcess = powershellRequest;
    powershellRequest.stdout.on('data', function (buf) { taskData.process.stdOut += buf.toString(); })
    powershellRequest.stderr.on('data', function (buf) { taskData.process.stdOut += buf.toString(); taskData.process.stdErr += buf.toString(); })
    powershellRequest.on('exit', (code, signal) => {
      taskData.process.endTime = new Date();      
      if (taskData.process.stdErr)
        console.log("error with command (" + taskData.process.id + "): " + taskData.process.stdErr);
      try {
        if (taskData.task.callback)
          taskData.task.callback(taskData.process.stdOut);
      } catch (e) {
        console.log("error with command (" + taskData.process.id + ") callback: " + e);
      }
      taskData.process.exitCode = code;
      taskData.process.childProcess = null;
      taskData.process.isActive = false;
    })
  }

  private setIconProgressBar() {
    var win = remote.getCurrentWindow();
    if (!win.isFocused()) {
      win.setProgressBar(1)
      return;
    }
  };

}

class Queue<T> {
  public store: T[] = [];
  push(val: T) {
    this.store.push(val);
  }
  pop(): T | undefined {
    return this.store.shift();
  }
  any(): boolean {
    return this.store.length > 0;
  }
}

class TaskData {
  constructor(public task: PowershellTask, public process: RunningProcess) { }

}