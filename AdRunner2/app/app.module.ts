import { AddFolderComponent } from "./Components/add-folder.component";
import { AppComponent } from "./Components/app.component";
import { BrowserModule } from "@angular/platform-browser";
import { CheckoutCommonComponent } from "./Components/checkout-common.component";
import { CheckoutComponent } from "./Components/checkout.component";
import { CheckoutsComponent } from "./Components/checkouts.components";
import { ConsoleOutputComponent } from "./Components/console-output.component";
import { FolderPannelComponent } from "./Components/folder-pannel.component";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { ProcessComponent } from "./Components/process.component";
import { ProcessPoolService } from "./services/process-pool.service";
import { SettingsService } from "./services/settings.service";

@NgModule({
  declarations: [
    CheckoutComponent, CheckoutsComponent,
    AppComponent, FolderPannelComponent,
    AddFolderComponent, ProcessComponent,
    ConsoleOutputComponent, CheckoutCommonComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [SettingsService, ProcessPoolService,],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
