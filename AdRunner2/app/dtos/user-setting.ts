import { Folder } from "./folder";

export class UserSettings {
    public folders: Folder[];
    public applicationVersion: string;
}