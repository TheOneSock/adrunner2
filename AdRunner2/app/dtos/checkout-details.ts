import { Folder } from "./folder";

export class CheckoutDetails {

    public checkoutFile:Folder;
    public checkoutVersion:string;
    public paskeTasks:PsakeTaskInfo[];
    public solutionFiles:Folder[];
    public executables:Folder[];
}

export class PsakeTaskInfo{
    public Name:string;
    public Description:string;
}