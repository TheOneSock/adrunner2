
import * as child from 'child_process';
import { Folder } from "./folder";
import { CheckoutComponent } from "../Components/checkout.component";

export class SvnAction {
    public constructor(name: string, callback?: () => void) {
        this.name = name;
        this.callback = callback;
    }
    public callback: () => void;
    public name: string;
    public action(folder: Folder): void { };
}

export class SvnSimpleAction extends SvnAction {
    public constructor(name: string, command: string, closeOnEnd: number, callback?: () => void) {
        super(name, callback);
        this.command = command;
        this.closeOnEnd = closeOnEnd
    }
    private command: string;
    private closeOnEnd: number;
    public action(folder: Folder): void {
        var foo: child.ChildProcess = child.exec('tortoiseproc /closeonend:' + this.closeOnEnd + ' /command:' + this.command + ' /path:"' + folder.Path + '"');
        foo.on('exit', () => {
            if (this.callback)
                this.callback();
        })
    };
}

export class SvnPatchApplyAction extends SvnAction {
    public constructor(callback?: () => void) {
        super("Patch Apply", callback);
    }
    public action(folder: Folder): void {
        var foo: child.ChildProcess = child.exec('TortoiseMerge /patchpath:"' + folder.Path + '"');
    };
}

export class SvnMergeFullAction extends SvnAction {
    public constructor(callback?: () => void) {
        super("Merge Full", callback);
    }
    public action(folder: Folder): void {
        var foo: child.ChildProcess = child.exec('tortoiseproc /closeonend:2 /command:update /path:"' + folder.Path + '" & tortoiseproc /closeonend:0 /command:merge /path:"' + folder.Path + '" & tortoiseproc /closeonend:0 /command:commit /path:"' + folder.Path + '"');
    };
}

export class SvnActionProvider {

    static getSvnActions(component: CheckoutComponent): SvnAction[] {

        return [
            new SvnSimpleAction("Commit", "commit", 0),
            new SvnSimpleAction("Update", "update", 2, () => component.refreshFields()),
            new SvnSimpleAction("Clean Up", "cleanup", 2),
            new SvnSimpleAction("Revert", "revert", 2, () => component.refreshFields()),
            new SvnSimpleAction("Merge", "merge", 2),
            new SvnMergeFullAction(() => component.refreshFields()),
            new SvnSimpleAction("Patch Create", "createpatch", 2),
            new SvnPatchApplyAction(() => component.refreshFields()),
            new SvnSimpleAction("Merge", "merge", 2, () => component.refreshFields()),
            new SvnSimpleAction("Log", "log", 0),
            new SvnSimpleAction("Repo Browser", "repobrowser", 0),
        ]
    }
}