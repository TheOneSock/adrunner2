import { Subject } from "rxjs/Subject";
import * as child from 'child_process';

var nextProcessId: number = 0;

export class RunningProcess {
    public constructor(name: string, checkoutId: number) {
        this.name = name;
        this.percentDone = 0;
        this.isActive = true;
        this.id = nextProcessId++;
        this.deleteOnSuccessful = false;
        this.checkoutId = checkoutId;
        this._isStarted =false;
    }
    public id: number;
    public checkoutId: number;
    public name: string;
    private _percentDone: number;
    private _stdOut: string;
    private _isActive: boolean;
    private _isStarted: boolean;
    private _stdErr: string;
    public deleteOnSuccessful: boolean;
    public exitCode: number;
    public startTime: Date;
    public endTime: Date;
    public childProcess:child.ChildProcess;
    private _duration: number;


    public get duration(): number {
        if (this.isActive)
            return null;
        if (!this._duration) {
            this._duration = Math.round((this.endTime.getTime() - this.startTime.getTime()) / 10) / 100;
        }
        return this._duration;
    }

    public get timeInformation(): string {
        let start = "Not started yet";
        if (this.startTime)
            start = ('0' + this.startTime.getHours()).slice(-2) + ":" + ('0' + this.startTime.getMinutes()).slice(-2) + ":" + ('0' + this.startTime.getSeconds()).slice(-2) + " -> ";
        let end = "";
        let duration = "";
        if (!this.isActive) {
            end = ('0' + this.endTime.getHours()).slice(-2) + ":" + ('0' + this.endTime.getMinutes()).slice(-2) + ":" + ('0' + this.endTime.getSeconds()).slice(-2);
            duration = " (" + this.duration + "s)"
        }
        return start + end + duration;
    }


    public set stdOut(value: string) {
        this._stdOut = value;
        this.stdoutSource.next(this.id);
    }
    public get stdOut(): string {
        return this._stdOut;
    }

    public set stdErr(value: string) {
        this._stdErr = value;
    }
    public get stdErr(): string {
        return this._stdErr;
    }

    public set isActive(b: boolean) {
        this._isActive = b
        this.isActiveSource.next(new ProcessArgs(this.id, this.checkoutId))
    }
    public get isActive(): boolean {
        return this._isActive;
    }

    public set isStarted(b: boolean) {
        this._isStarted = b
        this.isStartedSource.next(new ProcessArgs(this.id, this.checkoutId))
    }
    public get isStarted(): boolean {
        return this._isStarted;
    }


    public set percentDone(value: number) {
        this._percentDone = value;
        this.percentDoneSource.next(this.id);
    }

    public get percentDone(): number {
        return this._percentDone;
    }

    public get isSuccessful(): boolean {
        if (this.isActive)
            return false;

        if (this.stdErr)
            return false;

        if (this.exitCode) {
            if (this.exitCode != 0) {
                return false;
            }
        }

        return true;
    }

    public get isUnSuccessful(): boolean {
        return !this._isActive && !this.isSuccessful;
    }


    private isActiveSource = new Subject<ProcessArgs>();
    public isActiveChanged$ = this.isActiveSource.asObservable();
    private isStartedSource = new Subject<ProcessArgs>();
    public isStartedChanged$ = this.isStartedSource.asObservable();
    private stdoutSource = new Subject<number>();
    public stdoutChanged$ = this.stdoutSource.asObservable();
    private percentDoneSource = new Subject<number>();
    public percentDoneChanged$ = this.percentDoneSource.asObservable();
}

export class ProcessArgs {
    public constructor(processId: number, checkoutId: number) {
        this.checkoutId = checkoutId;
        this.processId = processId;
    }
    public checkoutId: number;
    public processId: number;
}