export class PowershellTask {
  public constructor(originCheckout: number, name: string, command: string, options?: PowershellTaskOptions) {
    this.name = name === name ? name : null;
    this.displayName = this.name;
    this.command = command;
    this.originCheckout = originCheckout;
    this.description = this.name;
    if(options){
      this.callback = options.callback ? options.callback : null;
      this.description = options.description ? options.description : this.name;
    }
  }

  public displayName:string;
  public name: string;
  public description: string;
  public command: string;
  public callback: (stdout: string) => void;
  public deleteProcessOnSuccessful: boolean
  public originCheckout: number;
  
}
export interface PowershellTaskOptions {
  description?: string;
  callback?: (stdout: string) => void;
}



export class PsakeTask extends PowershellTask {
  public constructor(originCheckout: number, name: string, buildPath: string, options?:PowershellTaskOptions) {
    super(
      originCheckout,
      'invoke-psake ' + name,
      'powershell -NoProfile -ExecutionPolicy Bypass -Command "&{ cd \'' + buildPath + '\';./start.ps1; invoke-psake ' + name +'; if ($psake.build_success -eq $false) { exit 1 } else { exit 0 } }"',
      options
    );
    this.displayName = name;
  }
}

//public constructor(name: string, alias: string, description: string, dependsOn: string) {