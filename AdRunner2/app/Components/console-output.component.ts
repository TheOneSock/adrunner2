import { Component, EventEmitter, Input, Output, OnInit, OnChanges, SimpleChanges, ChangeDetectorRef, OnDestroy, ViewChild, ElementRef, AfterViewChecked } from "@angular/core";
import { ProcessPoolService } from "../services/process-pool.service";
import { RunningProcess } from "../dtos/running-process";
import { Subscription } from "rxjs/Subscription";
import { clipboard } from 'electron'


@Component({
    selector: 'console-output',
    templateUrl: './html/console-output.component.html',
    styleUrls: ['./css/app.css']
})
export class ConsoleOutputComponent implements OnChanges, OnInit, OnDestroy {
    private watchProcessSub: Subscription;
    private processStdoutSub: Subscription;
    private processRemovedSub: Subscription;
    @ViewChild('scrollMe') private myScrollContainer: ElementRef;


    ngOnDestroy(): void {
        this.watchProcessSub.unsubscribe();
        this.processStdoutSub.unsubscribe();
        this.processRemovedSub.unsubscribe();
    }

    @Input() public get height(): number {
        return this.isVisible ? 400 : 20;
    }
    @Input() public isVisible: boolean = false;

    @Input() public currentProcess: RunningProcess;

    public constructor(private changeDetectorRef: ChangeDetectorRef, private processPoolService: ProcessPoolService) {

    }

    ngOnInit(): void {
        this.watchProcessSub =this.processPoolService.watchProcess$.subscribe(id => {
            this.currentProcess = this.processPoolService.GetProcess(id);
            if(!this.isVisible){
                 this.openHide();
            }

            this.changeDetectorRef.reattach();
            this.changeDetectorRef.detectChanges();
            this.changeDetectorRef.detach();
             this.scrollToBottom();
        })

        this.processStdoutSub = this.processPoolService.processStdout$.subscribe(id => {
            if (this.currentProcess != null && id === this.currentProcess.id) {
                this.changeDetectorRef.reattach();
                this.changeDetectorRef.detectChanges();
                this.changeDetectorRef.detach();
                 this.scrollToBottom();
            }
        })

       this.processRemovedSub = this.processPoolService.processRemoved$.subscribe(id => {
            if (this.currentProcess != null && id.processId === this.currentProcess.id) {
                this.isVisible = false
                this.currentProcess = null;
                this.changeDetectorRef.reattach();
                this.changeDetectorRef.detectChanges();
                this.changeDetectorRef.detach();
                 this.scrollToBottom();
            }
        })
        this.scrollToBottom();
    }

    private scrollToBottom(): void {
        //https://stackoverflow.com/questions/35232731/angular2-scroll-to-bottom-chat-style
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }                 
    }

    ngOnChanges(changes: SimpleChanges): void {
    }

    public copyToClipboard(){
        if (this.currentProcess != null)
            clipboard.writeText(this.currentProcess.stdOut);
    }

    public openHide() {
        this.isVisible = this.currentProcess != null ? !this.isVisible : this.isVisible;
        this.changeDetectorRef.reattach();
        this.changeDetectorRef.detectChanges();
        this.changeDetectorRef.detach();
         this.scrollToBottom();
    }
}