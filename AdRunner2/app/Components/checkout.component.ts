import { Component, EventEmitter, Input, ChangeDetectorRef, Output, OnChanges, SimpleChanges, OnInit, OnDestroy } from "@angular/core";
import * as child from 'child_process';
import * as fs from 'fs'
import * as path from 'path';
import { remote } from 'electron';

import { Folder } from "../dtos/folder";
import { SvnAction, SvnActionProvider } from "../dtos/svn-action";
import { PowershellTask, PsakeTask } from "../dtos/psake-task";
import { RunningProcess } from "../dtos/running-process";
import { ProcessPoolService } from "../services/process-pool.service";
import { Subscription } from "rxjs/Subscription";
import { PowershellCommandRunner } from "../services/powershell-comand-runner";
import { SettingsService } from "../services/settings.service";
import { CheckoutDetails } from "../dtos/checkout-details";

var nextCheckoutId: number = 0;

@Component({
  selector: 'checkout',
  templateUrl: './html/checkout.component.html',
})
export class CheckoutComponent implements OnChanges, OnInit, OnDestroy {

  public constructor(private changeDetectorRef: ChangeDetectorRef, private processPoolService: ProcessPoolService, private settingsService: SettingsService) {
    this._checkoutId = nextCheckoutId++;
    this.powershellCommandRunner = PowershellCommandRunner.getInstance(processPoolService);
  }

  private processRemovedsub: Subscription;
  private processDonesub: Subscription;
  private processAddedsub: Subscription;
  private _checkoutId: number;
  private powershellCommandRunner: PowershellCommandRunner;
 
  @Input() public checkoutFolder: Folder;
  @Input() public powershellTasks: PowershellTask[] = [];
  public svnActions: SvnAction[] = SvnActionProvider.getSvnActions(this);
  @Input() public runningProcesses: RunningProcess[] = [];
  @Input() public runningProcessIds: Set<number> = new Set<number>();
  @Input() public solutionFiles: Folder[] = [];
  @Input() public executables: Folder[] = [];
  @Input() public version: string = "0.0.0";
  @Input() public implementationConfigs: Folder[] = [];
  @Input() public selectedImplementationConfig: Folder;
  
  public startPowershellTask(task: PowershellTask) {
    let process = this.powershellCommandRunner.startPowershellTask(task);

    this.runningProcesses.push(process);
    this.runningProcessIds.add(process.id);

    this.changeDetectorRef.reattach();
    this.changeDetectorRef.detectChanges();
    this.changeDetectorRef.detach();
  }

  public openExplorer(): void {
    var foo: child.ChildProcess = child.exec('explorer "' + this.checkoutFolder.Path + '"');
  }

  public openConsole(): void {
    var foo: child.ChildProcess = child.exec('start powershell -NoExit -Command "Set-Location \'' + this.checkoutFolder.Path + '\'"');
  }

  private setIISAndCSProj(): void {
    let task = new PowershellTask(this._checkoutId,
      "Set IIS and csproj",
      PowershellCommandRunner.powershellCommandPrefix+'"cd \'' + this.settingsService.scriptPath + '\';./setupCheckout.ps1 -checkoutFolder \'' + this.checkoutFolder.Path + '\'"')
    this.startPowershellTask(task);
  };

  private getImplementationConfigs() {
    let promise = new Promise(() => {
      this.implementationConfigs = this.settingsService.getImplementationConfigFiles();
      if (this.implementationConfigs.length > 0)
        this.selectedImplementationConfig = this.implementationConfigs[0];
      this.changeDetectorRef.reattach();
      this.changeDetectorRef.detectChanges();
      this.changeDetectorRef.detach();
    });
  }

  public selectConfig(f: Folder) {
    this.selectedImplementationConfig = f;
    this.changeDetectorRef.reattach();
    this.changeDetectorRef.detectChanges();
    this.changeDetectorRef.detach();
  }

  public applySelectedConfig() {
    let task = new PowershellTask(this._checkoutId,
      "Apply Selected Config",
      PowershellCommandRunner.powershellCommandPrefix +'"cd \'' + this.settingsService.scriptPath + '\';./copyImplementationConfig.ps1 -newConfig \'' + this.selectedImplementationConfig.Path + '\' -checkoutFolder \'' + this.checkoutFolder.Path + '\'"')
    task.deleteProcessOnSuccessful = true;
    this.startPowershellTask(task);
  }

  private getCheckoutInformation(): void {
    let task = new PowershellTask(this._checkoutId,
      "Get checkout info",
      PowershellCommandRunner.powershellCommandPrefix +'"cd \'' + this.settingsService.scriptPath + '\';./getCheckoutInformation.ps1 -checkoutFolder \'' + this.checkoutFolder.Path + '\'"',
      {callback:this.getCheckoutInformationCallback})
    task.deleteProcessOnSuccessful = true;
    this.startPowershellTask(task);
  }

  public getCheckoutInformationCallback = (jsonString: string): void => {
    let buildPath = path.join(this.checkoutFolder.Path, 'build');
    if (!fs.existsSync(buildPath)) {
       buildPath = path.join(this.checkoutFolder.Path, '.build');
    }
    let checkoutDetails = JSON.parse(jsonString) as CheckoutDetails;
    
    this.version = checkoutDetails.checkoutVersion;
    
    this.solutionFiles = checkoutDetails.solutionFiles;

    let matches: PowershellTask[] = [];
    checkoutDetails.paskeTasks.forEach(element => {
      if (element.Name === "?")
        return;
      if (element.Name.startsWith("Out-"))
        return;
      if (element.Name.startsWith("Resolve-"))
        return;
      matches.push(new PsakeTask(this._checkoutId, element.Name, buildPath, { description: element.Description}));
    });
    this.powershellTasks = matches;

    this.executables = checkoutDetails.executables;

  }

  public openVisualStudio(solution:Folder) {
    child.exec(PowershellCommandRunner.powershellCommandPrefix + '"start \'' + solution.Path + '\'"');
  }

  public runExecutable(executable:Folder){
    child.exec(PowershellCommandRunner.powershellCommandPrefix + '"start \'' + executable.Path + '\'"')
  }

  public refreshFields() {
    console.log("refreshing checkout " + this.checkoutFolder.Name);
    this.getCheckoutInformation();
    this.getImplementationConfigs();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['checkoutFolder']) {
      this.refreshFields();
    }
  }

  ngOnDestroy(): void {
    this.processAddedsub.unsubscribe();
    this.processDonesub.unsubscribe();
    this.processRemovedsub.unsubscribe();
  }

  ngOnInit(): void {
    this.processAddedsub = this.processPoolService.processAdded$.subscribe(pid => {
      if (this.runningProcessIds.has(pid)) {
        this.changeDetectorRef.reattach();
        this.changeDetectorRef.detectChanges();
        this.changeDetectorRef.detach();
      }
    });
    this.processDonesub = this.processPoolService.processDone$.subscribe(pid => {
      if (this.runningProcessIds.has(pid.processId)) {
        this.changeDetectorRef.reattach();
        this.changeDetectorRef.detectChanges();
        this.changeDetectorRef.detach();
      }
    });
    this.processRemovedsub = this.processPoolService.processRemoved$.subscribe(pid => {
      if (this.runningProcessIds.has(pid.processId)) {

        this.runningProcessIds.delete(pid.processId);
        let index = this.runningProcesses.findIndex(p => p.id == pid.processId);

        if (index > -1) {
          this.runningProcesses.splice(index, 1);
        }

        this.changeDetectorRef.reattach();
        this.changeDetectorRef.detectChanges();
        this.changeDetectorRef.detach();
      }
    });
  }


}



