import * as path from "path";
import {
  Component,
  EventEmitter,
  Input,
  Output
  } from "@angular/core";
import { Folder } from "../dtos/folder";
import { remote } from "electron";

@Component({
  selector: 'add-folder',
  templateUrl: './html/add-folder.component.html',
})
export class AddFolderComponent {
  @Input() public folders: Folder[];
  @Output() public addFolderEE = new EventEmitter<Folder>();

  public openExplorer(): void {
    //http://ourcodeworld.com/articles/read/106/how-to-choose-read-save-delete-or-create-a-file-with-electron-framework
    remote.dialog.showOpenDialog({
      title: "Select a folder",
      properties: ["openDirectory"]
    }, (folderPaths) => {
      if (folderPaths === undefined) {
        console.log("No destination folder selected");
        return;
      } else {
        let folder = new Folder();
        folder.Name = path.basename(folderPaths[0]);
        folder.Path = folderPaths[0];
        this.addFolderEE.emit(folder);
        console.log("Folder added: "+JSON.stringify(folder));
      }
    });
  }
}