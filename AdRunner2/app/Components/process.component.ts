import { Component, EventEmitter, Input, Output, OnChanges, SimpleChanges, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { RunningProcess } from "../dtos/running-process";
import { Subscription } from "rxjs/Subscription";
import { ProcessPoolService } from "../services/process-pool.service";
import * as child from 'child_process';


@Component({
    selector: 'process',
    templateUrl: './html/process.component.html',
})
export class ProcessComponent implements OnChanges, OnInit, OnDestroy {
    private processRemovedsub: Subscription;
    private processDonesub: Subscription;
    private percentDoneSub: Subscription;


    ngOnDestroy(): void {

        this.processDonesub.unsubscribe();
        this.processRemovedsub.unsubscribe();
    }

    ngOnInit(): void {
        this.processDonesub = this.processPoolService.processDone$.subscribe(pid => {
            if (this.process != null && this.process.id === pid.processId) {
                this.changeDetectorRef.reattach();
                this.changeDetectorRef.detectChanges();
                this.changeDetectorRef.detach();
            }
        });

        this.processRemovedsub = this.processPoolService.processRemoved$.subscribe(pid => {
            if (this.process != null && this.process.id === pid.processId) {
                this.process = null;
                this.changeDetectorRef.reattach();
                this.changeDetectorRef.detectChanges();
                this.changeDetectorRef.detach();
            }
        });

        this.percentDoneSub = this.processPoolService.processPercentDone$.subscribe(pid => {
            if (this.process != null && this.process.id === pid) {
                this.changeDetectorRef.reattach();
                this.changeDetectorRef.detectChanges();
                this.changeDetectorRef.detach();
            }
        });
        this.percentDoneSub = this.processPoolService.processStarted$.subscribe(pid => {
            if (this.process != null && this.process.id === pid.processId) {
                this.changeDetectorRef.reattach();
                this.changeDetectorRef.detectChanges();
                this.changeDetectorRef.detach();
            }
        });
    }


    public constructor(private changeDetectorRef: ChangeDetectorRef, private processPoolService: ProcessPoolService) { }

    @Input() process: RunningProcess;

    ngOnChanges(changes: SimpleChanges): void {
    }

    public ShowLog(): void {
        this.processPoolService.WatchProcess(this.process.id);
    }
    public killProcess(): void {
        if (this.process.childProcess){
            child.execSync('Taskkill /PID ' + this.process.childProcess.pid +' /F /T');
            
        }
    }
    public DeleteProcess(): void {
        let id = this.process.id;
        this.process = null;
        this.processPoolService.RemoveProcess(id);
    }

}