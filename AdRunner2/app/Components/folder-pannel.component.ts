import * as child from 'child_process';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output
  } from '@angular/core';
import { Folder } from '../dtos/folder';

@Component({
  selector: 'folder-pannel',
  templateUrl: './html/folder-pannel.component.html',
})
export class FolderPannelComponent{
  @Input() public folder: Folder;
  @Input() public selected: boolean;
  @Output() public removeFolderEE = new EventEmitter<Folder>();
  @Output() public selectFolderEE = new EventEmitter<Folder>();
  

  public openExplorer(): void {
    var foo: child.ChildProcess = child.exec('explorer "' + this.folder.Path + '"');
  }

  public openConsole(): void {
    var foo: child.ChildProcess = child.exec('start powershell -NoExit -Command "Set-Location \'' + this.folder.Path + '\'"');
  }

  public removeFolder(): void {
    this.removeFolderEE.emit(this.folder);
  }

  public selectFolder(): void {
    this.selectFolderEE.emit(this.folder);
  }
}