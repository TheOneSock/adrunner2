import * as fs from 'fs';
import * as path from 'path';
import {
    Component,
    Input,
    OnChanges,
    Output,
    SimpleChanges
} from '@angular/core';
import { Folder } from '../dtos/folder';
import { ProcessPoolService } from "../services/process-pool.service";


@Component({
    selector: 'checkout-common',
    templateUrl: './html/checkout-common.component.html',
})
export class CheckoutCommonComponent {
    public constructor(private processPoolService: ProcessPoolService){

    }

    public removeFinishedProcesses(){
        this.processPoolService.removeFinishedProcesses();
    }
    
}