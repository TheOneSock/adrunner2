import {
  ApplicationRef,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { Folder } from '../dtos/folder';
import { SettingsService } from '../services/settings.service';
import { UserSettings } from '../dtos/user-setting';



@Component({
  selector: 'my-app',
  templateUrl: './html/app.component.html',
  styleUrls: ['./css/app.css']
})
export class AppComponent implements OnInit {
  public constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _settingsService: SettingsService) { }

  @Input() public userSettings: UserSettings;
  @Output() public selectedFolder: Folder;

  public ngOnInit(): void {
    this.resetData();
    this.selectedFolder = this.userSettings.folders.length > 0 ?
      this.selectedFolder = this.userSettings.folders[0] :
      this.selectedFolder = null;
  }

  public onFolderAdded(folder: Folder) {
    if (!folder)
      return;
    this._settingsService.addFolder(folder);
    this.resetData();

    this._changeDetectorRef.reattach();
    this._changeDetectorRef.detectChanges();
    this._changeDetectorRef.detach();
  }

  public onFolderRemoved(folder: Folder) {
    this._settingsService.removeFolder(folder);
    this.resetData();

    this._changeDetectorRef.reattach();
    this._changeDetectorRef.detectChanges();
    this._changeDetectorRef.detach();
  }

  public onFolderSelected(folder: Folder) {

    this.selectedFolder = folder;
    if (this.selectedFolder) {
      let f = this.selectedFolder
      let index = this.userSettings.folders.find(function (value, index): boolean {
        return f === value
      });
      if (index === undefined)
        this.selectedFolder = null;
    }
    else if (this.userSettings.folders.length > 0) {
      this.selectedFolder = this.userSettings.folders[0]
    } else {
      this.selectedFolder = null;
    }

    this._changeDetectorRef.reattach();
    this._changeDetectorRef.detectChanges();
    this._changeDetectorRef.detach();
  }

  private resetData(): void {
    this.userSettings = this._settingsService.getUserSettings();
  }
}


