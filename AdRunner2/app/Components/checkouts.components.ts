import * as fs from 'fs';
import * as path from 'path';
import {
    Component,
    Input,
    OnChanges,
    Output,
    SimpleChanges
} from '@angular/core';
import { Folder } from '../dtos/folder';
import { remote } from 'electron';
import * as child from 'child_process';
import { SettingsService } from "../services/settings.service";
import { PowershellCommandRunner } from "../services/powershell-comand-runner";

@Component({
    selector: 'checkouts',
    templateUrl: './html/checkouts.component.html',
})
export class CheckoutsComponent implements OnChanges {
    @Input() public folder: Folder;
    @Output() public checkoutFolders: Folder[] = [];

    public constructor(private settingsService: SettingsService) {
    }

    ngOnChanges(changes: SimpleChanges): void {


        if (changes['folder']) {

            this.checkoutFolders = fs.readdirSync(this.folder.Path)
                .filter(file => fs.lstatSync(path.join(this.folder.Path, file))
                    .isDirectory())
                .map(file => {
                    let f = new Folder();
                    f.Name = file;
                    f.Path = path.join(this.folder.Path, file);
                    return f;
                });

            let folderNames = child.execSync(PowershellCommandRunner.powershellCommandPrefix +'"cd \'' + this.settingsService.scriptPath + '\';./getCorrectCheckoutsFromFolder.ps1 -checkoutFolder \'' + this.folder.Path + '\'', { encoding: 'utf8' });

            let folders: Folder[] = [];
            let regex = /"(.*?)"/g;
            let match = regex.exec(folderNames);
            while (match != null) {
                let f = new Folder();
                f.Name = match[1];
                f.Path = path.join(this.folder.Path, f.Name);
                folders.push(f);
                match = regex.exec(folderNames);
            }

            this.checkoutFolders = folders;



        }
    }
}