Param([Parameter(Mandatory=$true)][string]$newConfig,[Parameter(Mandatory=$true)][string]$checkoutFolder)

Import-Module (Join-Path $PSScriptRoot "common.psm1")

$version = Get-CheckoutVersion $checkoutFolder

if(-not (Test-Path $checkoutFolder))
{
    Write-Error "folder $checkoutFolder does not exist"
    exit 1
}elseif(-not (Test-Path "$checkoutFolder\Implementations")){
    #fail gracefully
    Write-Color "folder  $checkoutFolder\Implementations does not exist, this is not a proper adinsure checkout" -Color Red
    exit 0
}

if(-not (Test-Path $newConfig)){
    Write-Error "Config file $newConfig does not exist"
    exit 1
}

$implementations =  Get-Implementations $checkoutFolder

foreach ($implenentationName in $implementations) {

    $file = Join-Path "$checkoutFolder/Implementations/$implenentationName/Applications/AdInsure.$implenentationName.WebService/" (Get-ImplementationConfigLocation $version)
    Copy-Item $newConfig $file -Verbose

}