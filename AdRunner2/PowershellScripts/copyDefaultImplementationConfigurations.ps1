Param([Parameter(Mandatory=$true)][string]$defaultLocation,[Parameter(Mandatory=$true)][string]$userlocation)

if(-not (Test-Path $defaultLocation))
{
    Write-Error "folder $defaultLocation does not exist"
    exit 1
}

if(-not (Test-Path $userlocation)){
    Write-Error "folder $userlocation does not exist"
    exit 1
}

Copy-Item $defaultLocation $userlocation -recurse -force
exit 0