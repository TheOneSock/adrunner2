Param([Parameter(Mandatory = $true)][string]$checkoutFolder)

Import-Module (Join-Path $PSScriptRoot "common.psm1")

Import-Module WebAdministration


if (-not (Test-Path $checkoutFolder)) {
    Write-Error "folder $checkoutFolder does not exist"
    exit 1
}
elseif (-not (Test-Path "$checkoutFolder\Implementations")) {
    #fail gracefully
    Write-Color "folder  $checkoutFolder\Implementations does not exist, this is not a proper adinsure checkout" -Color Red
    exit 0
}


$root = $PSScriptRoot
$checkoutName = Split-Path $checkoutFolder -Leaf
$prefix = "Adacta.IIS.WebService"

#implementations
$implementations = Get-Implementations $checkoutFolder

$version = Get-CheckoutVersion $checkoutFolder

foreach ($implenentationName in $implementations) { 
      
    Write-Color "Creating or Finding app-pool and site for implementation ", $implenentationName -Color Magenta, Green

      
    $iisAppPoolName = "$($prefix).$($checkoutName)"
    $appName = "$($prefix).$($checkoutName)-$($implenentationName)"
    $directoryPath = "$($checkoutFolder)\Implementations\$($implenentationName)\Applications\Adinsure.$($implenentationName).WebService"
    $clientAppConfig = Join-Path "$($checkoutFolder)\Implementations\$($implenentationName)\Applications\Adinsure.$($implenentationName).ClientApp\" (Get-ImplementationConfigLocation $version)

    if (-not (Test-Path $directoryPath)) {
        Write-Color "WebService path does not exist $directoryPath" -Color Red
        continue
    }

    #navigate to the app pools root
    cd IIS:\AppPools\

    #check if the app pool exists
    if (!(Test-Path $iisAppPoolName -pathType container)) {
        Write-Color "Creating app-pool ", $iisAppPoolName -Color White, Green
        #create the app pool
        $appPool = New-Item $iisAppPoolName
        $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value "v4.0" | Set-ItemProperty -Name "managedPipelineMode" -Value "0"
    }
    else {
        Write-Color  "Using app-pool ", $iisAppPoolName -Color White, Green
    }

    $appPath = "IIS:\Sites\Default Web Site\"

    if ((Test-Path $appPath$appName) -eq 0 -and (Get-WebApplication -Name $appName) -eq $null ){  
        Write-Color  "Creating site ", $appName -Color White, Green
        New-WebApplication -Name $appName -ApplicationPool $iisAppPoolName -Site "Default Web Site" -PhysicalPath $directoryPath > $null
    } elseif((Get-WebApplication -Name $appName).applicationPool -ne $iisAppPoolName){
        Write-Color  "Fixing site app pool ", $appName -Color White, Green
        Set-ItemProperty $appPath$appName applicationPool $iisAppPoolName 
    }
    else {
        Write-Color "Using site ", $appName -Color White, Green
    }

    cd $root

    $CSProjs = Get-ChildItem $directoryPath -Filter '*.csproj' | % {$_.FullName}

    foreach ($CSProj in $CSProjs) {
        $userCSProj = "$CSProj.user"
        $CSProjName = Split-Path $CSProj -Leaf
        $userCSProjName = Split-Path $userCSProj -Leaf

        Write-Color "Creating ", $userCSProjName, " with IISUrl = http://localhost/", $appName -Color White, Green, White, Green

        $content = "<?xml version=""1.0"" encoding=""utf-8""?>
          <Project ToolsVersion=""14.0"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
          <PropertyGroup>
          <UseIISExpress>false</UseIISExpress>
          <IISExpressSSLPort />
          <IISExpressAnonymousAuthentication />
          <IISExpressWindowsAuthentication />
          <IISExpressUseClassicPipelineMode />
          <UseGlobalApplicationHostFile />
          </PropertyGroup>
          <ProjectExtensions>
          <VisualStudio>
          <FlavorProperties GUID=""{349c5851-65df-11da-9384-00065b846f21}"">
          <WebProjectProperties>
          <StartPageUrl>
          </StartPageUrl>
          <StartAction>CurrentPage</StartAction>
          <AspNetDebugging>True</AspNetDebugging>
          <SilverlightDebugging>False</SilverlightDebugging>
          <NativeDebugging>False</NativeDebugging>
          <SQLDebugging>False</SQLDebugging>
          <ExternalProgram>
          </ExternalProgram>
          <StartExternalURL>
          </StartExternalURL>
          <StartCmdLineArguments>
          </StartCmdLineArguments>
          <StartWorkingDirectory>
          </StartWorkingDirectory>
          <EnableENC>True</EnableENC>
          <AlwaysStartWebServerOnDebug>True</AlwaysStartWebServerOnDebug>
          <UseIIS>True</UseIIS>
          <AutoAssignPort>True</AutoAssignPort>
          <DevelopmentServerPort>59157</DevelopmentServerPort>
          <DevelopmentServerVPath>/</DevelopmentServerVPath>
          <IISUrl>http://localhost/$appName</IISUrl>
          <OverrideIISAppRootUrl>True</OverrideIISAppRootUrl>
          <IISAppRootUrl>http://localhost/$appName</IISAppRootUrl>
          <NTLMAuthentication>False</NTLMAuthentication>
          <UseCustomServer>False</UseCustomServer>
          <CustomServerUrl>
          </CustomServerUrl>
          </WebProjectProperties>
          </FlavorProperties>
          </VisualStudio>
          </ProjectExtensions>
          </Project>"

        $appContent = "
          <appSettings>
          <add key=""AdInsure:Settings:WebService:InternetRoot"" value=""http://localhost/$appName""/>
          <add key=""AdInsure:Settings:WebService:Root"" value=""http://localhost/$appName""/>
          <add key=""AdInsure:Settings:WebService:AlternativeRoot"" value=""http://localhost/$appName""/>
          </appSettings>
          "
        if (-not (Test-Path $userCSProj) -or -not ((Get-Content $userCSProj | % {$_ -match $appName}) -contains $True)) {
            Write-Color "Correcting ", $userCSProj, " with IISUrl http://localhost/", $appName -Color White, Green, White, Green
            [System.IO.File]::WriteAllLines($userCSProj, $content)
        }
        else {
            Write-Color $userCSProj, " already has IISUrl http://localhost/", $appName -Color Green, White, Green
        }
        if (-not (Test-Path $clientAppConfig) -or -not ((Get-Content $clientAppConfig | % {$_ -match $appName}) -contains $True)) {
            Write-Color "Correcting ", $clientAppConfig, " with IISUrl http://localhost/", $appName -Color White, Green, White, Green
            [System.IO.File]::WriteAllLines($clientAppConfig, $appContent)
        }
        else {
            Write-Color $clientAppConfig, " already has IISUrl http://localhost/", $appName -Color Green, White, Green
        }
        if (-not (Test-Path $CSProj) -or -not ((Get-Content $CSProj | % {$_ -match "<SaveServerSettingsInUserFile>True"}) -contains $True)) {
            Write-Color "Correcting ", $CSProjName, " with SaveServerSettingsInUserFile=True" -Color White, Green, White
            $csprojContent = [IO.File]::ReadAllText($CSProj)
            $csprojContent = [regex]::replace($csprojContent, '<WebProjectProperties[\s\S]*WebProjectProperties>', '<WebProjectProperties><SaveServerSettingsInUserFile>True</SaveServerSettingsInUserFile></WebProjectProperties>', 'Multiline');
            [System.IO.File]::WriteAllLines($CSProj, $csprojContent)

        }
        else {
            Write-Color $CSProjName, " already has  SaveServerSettingsInUserFile=True" -Color Green, White
        }
               
        Write-Color "Adding ", $CSProjName, " to ignore-on-commit changelist" -Color White, Green, White

        svn changelist "ignore-on-commit" $CSProj

        ""

    }
}

exit 0













