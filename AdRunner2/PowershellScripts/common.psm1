function Write-Color([String[]]$Text, [ConsoleColor[]]$Color = "White", [int]$StartTab = 0, [int] $LinesBefore = 0, [int] $LinesAfter = 0) {
    $DefaultColor = $Color[0]
    if ($LinesBefore -ne 0) {  for ($i = 0; $i -lt $LinesBefore; $i++) { Write-Host "`n" -NoNewline } } # Add empty line before
    if ($StartTab -ne 0) {  for ($i = 0; $i -lt $StartTab; $i++) { Write-Host "`t" -NoNewLine } }  # Add TABS before text
    if ($Color.Count -ge $Text.Count) {
        for ($i = 0; $i -lt $Text.Length; $i++) { Write-Host $Text[$i] -ForegroundColor $Color[$i] -NoNewLine } 
    }
    else {
        for ($i = 0; $i -lt $Color.Length ; $i++) { Write-Host $Text[$i] -ForegroundColor $Color[$i] -NoNewLine }
        for ($i = $Color.Length; $i -lt $Text.Length; $i++) { Write-Host $Text[$i] -ForegroundColor $DefaultColor -NoNewLine }
    }
    Write-Host
    if ($LinesAfter -ne 0) {  for ($i = 0; $i -lt $LinesAfter; $i++) { Write-Host "`n" } }  # Add empty line after
}

function Get-Implementations($checkoutFolder){
    return Get-ChildItem (Join-Path $checkoutFolder "Implementations") | ? { $_.PSIsContainer -and -not $_.Name.StartsWith(".") -and ($_.Name -ne "TestResults" ) -and ($_.Name -ne "Basic.RU" )} | % {$_.Name}    
}

function Get-CheckoutVersion{
    [OutputType([System.Version])]
    Param (
        [parameter(Mandatory=$true)]
        [String] $checkoutFolder
    )
    $path= Join-Path $checkoutFolder "build/GlobalAssemblyInfo.cs"
    if (-not (Test-Path $path)) { 
        $path= Join-Path $checkoutFolder ".build/GlobalAssemblyInfo.cs"
    }
    return [IO.File]::ReadAllText($path) | select-string "\[assembly: AssemblyVersion\(""(.*?)""\)]" | %{$_.matches[0].groups[1].value}
}

function Get-ImplementationConfigLocation([System.Version]$version){
    if($version -lt [System.Version]"2.8.146"){
        return "implementation.config"
    }else{
        return "conf/ImplementationConfiguration.config"
    }
}

Export-ModuleMember -Function "Get-Implementations"
Export-ModuleMember -Function "Write-Color"
Export-ModuleMember -Function "Get-ImplementationConfigLocation"
Export-ModuleMember -Function "Get-CheckoutVersion"