
Param([Parameter(Mandatory = $true)][string]$checkoutFolder)

Import-Module (Join-Path $PSScriptRoot "common.psm1")

function ParsePsakeTask($listViewFieldList) {
    $task = New-Object -TypeName PSObject
    $listViewFieldList | % { Add-Member -InputObject $task -MemberType NoteProperty  -Name $_.propertyName -Value $_.formatPropertyField.propertyValue}
    return $task
}

function Get-PaskeTasks {
    $buildPath = Join-Path $checkoutFolder "build"

    if (Test-Path $buildPath) {
        cd $buildPath
        (& "modules\PSake\psake.ps1" "default.ps1" "-detailedDocs") 2>&1 | out-null
        ($docs = Invoke-Psake -detailedDocs| ? {$_.formatEntryInfo} | % { ParsePsakeTask $_.formatEntryInfo.listViewFieldList} | ? {-not $_.Alias} | Select-Object -Property @("Name", "Description")) 2>&1 | out-null
        cd $PSScriptRoot
        return $docs
    }else{
        $buildPath = Join-Path $checkoutFolder ".build"
        cd $buildPath
        (& ".buildProcess\modules\PSake\psake.ps1" "default.ps1" "-detailedDocs") 2>&1 | out-null
        ($docs = Invoke-Psake -detailedDocs| ? {$_.formatEntryInfo} | % { ParsePsakeTask $_.formatEntryInfo.listViewFieldList} | ? {-not $_.Alias} | Select-Object -Property @("Name", "Description")) 2>&1 | out-null
        cd $PSScriptRoot
        return $docs
    }
}

#Finds al relevant .sln files in the checkout
function Get-SolutionFiles {
    [OutputType([string[]])]
    $slns = @()
    foreach ($implenentationName in Get-Implementations $checkoutFolder) {
        $slns = $slns + (@(Get-ChildItem -Path "$($checkoutFolder)\Implementations\$($implenentationName)" -Filter *.sln -Recurse)| % {$_.FullName })
    }

    $buildPath = Join-Path $checkoutFolder "build"

    if (-not (Test-Path $buildPath)) { 
        $buildPath = Join-Path $checkoutFolder ".build"
    }

    $slns = $slns + (@(Get-ChildItem -Path $buildPath -Filter *.sln -Recurse)| % {$_.FullName})
    return $slns
}


function Get-Executables {
    $implementations = @(Get-Implementations $checkoutFolder)
    $items = @()
    foreach ($implementation in $implementations) {
        $path = Join-Path $checkoutFolder "\Implementations\$($implementation)\Applications\AdInsure.$($implementation).ClientApp\bin\Debug\Adacta.IIS.Client.exe"
        $i = @{
            "Name"        = Split-Path $path -Leaf;
            "Path"        = $path;
            "Description" = "$implementation - $(Split-Path $path -Leaf)";
        }
        $item = New-Object -TypeName PSObject -Property $i

        $items = $items + @($item) 
    }
    $path = Join-Path $checkoutFolder "\bin\TestClient\Adacta.Service.TestClient.exe"
    $i = @{
        "Name"        = Split-Path $path -Leaf;
        "Path"        = $path;
        "Description" = "TestClient";
    }
    $item = New-Object -TypeName PSObject -Property $i
    $items = $items + @($item) 

    $path = Join-Path $checkoutFolder "\AdInsurePowerTool.bat"
    $i = @{
        "Name"        = Split-Path $path -Leaf;
        "Path"        = $path;
        "Description" = "AdInsurePowerTool";
    }
    $item = New-Object -TypeName PSObject -Property $i
    $items = $items + @($item) 

    $path = Join-Path $checkoutFolder "\bin\Merger.TestClient\Adacta.AdInsure.Merger.TestClient.exe"
    $i = @{
        "Name"        = Split-Path $path -Leaf;
        "Path"        = $path;
        "Description" = "Merger";
    }
    $item = New-Object -TypeName PSObject -Property $i
    $items = $items + @($item) 
    

    return $items
}

function PathToFolder($path) {
    $i = @{
        "Name" = Split-Path $path -Leaf;
        "Path" = $path;
    }
    $o = New-Object -TypeName PSObject -Property $i
    return $o;
}
 

$info = @{
    "checkoutFile"    = $checkoutFolder| % {PathToFolder $_}
    "checkoutVersion" = (Get-CheckoutVersion $checkoutFolder);
    "paskeTasks"      = @(Get-PaskeTasks);
    "solutionFiles"   = @(Get-SolutionFiles)| % {PathToFolder $_};
    "executables"     = @(Get-Executables);
}


$object = New-Object -TypeName PSObject -Property $info 
$object | ConvertTo-Json 
