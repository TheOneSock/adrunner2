Param([Parameter(Mandatory=$true)][string]$checkoutFolder)

# .build exists
# version can be read and is > 2.7.
function TestVersion($rootPath){

    $path = Join-Path $rootPath "\build\GlobalAssemblyInfo.cs"

    if(-not (Test-Path $path)){
        $path = Join-Path $rootPath "\.build\GlobalAssemblyInfo.cs"
        if(-not (Test-Path $path)){
            return $False
        }
    }

    Try{
        $content = Get-Content $path;
        $m = ([regex]"\[assembly: AssemblyVersion\(.(\d+)\.(\d+).*.\)\]").Match($content);
        if($m){ 
            [int]$major = [convert]::ToInt32($m.Captures[0].Groups[1].value, 10);
            [int]$minor = [convert]::ToInt32($m.Captures[0].Groups[2].value, 10);

            if($major -and ($major -eq 2)){
                if($minor -and ($minor -gt 6)){
                    return $True
                }
            }elseif($major -and  ($major -gt 2)){
                return $True
            }
        }
        return $False
    }
    Catch {
        return $False
    }
}

$implementations = Get-ChildItem $checkoutFolder | ?{ $_.PSIsContainer } | %{ $_.Name }| ?{ TestVersion  "$checkoutFolder\$($_)\" } | %{ """$_""" } 
Write-host $implementations 

